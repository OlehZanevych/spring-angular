# Spring + Angular
This is my primitive web application. It consists of three parts: scripts needed to initialize the database (database), Web Service designed with Spring Boot (service-app), and User Interface written on Angular 4 (ui-app).

## How do I get setup and run?
You should do few simple things:

1. Clone repository (git clone https://OlehZanevych@bitbucket.org/OlehZanevych/spring-angular.git)

2. Create database on MySQL using required properties from pom.xml in service-app folder

3. Initialize database schema and seed it with test data by importing files structure.sql and test_data.sql from database folder

4. Building Web Service application (go into service-app and run mvn clean install)

5. Running Web Service application (in same folder run mvn spring-boot:run)

6. Building User Interface application (go into ui-app and run npm install)

7. Running User Interface application (in same folder run ng serve -o). If you did everything right then in a few seconds in your browser will open the home page of the application http://localhost:4200/main