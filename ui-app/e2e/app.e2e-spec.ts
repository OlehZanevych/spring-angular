import { TestTaskUiAppPage } from './app.po';

describe('test-task-ui-app App', () => {
  let page: TestTaskUiAppPage;

  beforeEach(() => {
    page = new TestTaskUiAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
