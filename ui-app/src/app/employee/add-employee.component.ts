import { Component, OnInit} from '@angular/core';
import { Response } from '@angular/http';
import { HttpService } from '../http.service';
import { Observable } from 'rxjs/Rx';
import { PagedResult } from '../paged-result';
import { Department } from '../department/department';
import { Employee } from './employee';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-edit-employee.component.html',
  styleUrls: ['./add-edit-employee.component.css'],
  providers: [HttpService]
})
export class AddEmployeeComponent implements OnInit {
  title = 'Add Employee';

  submitOperation = 'Add';

  httpStatus: number;

  departmentsData: PagedResult<Department>;

  employee = new Employee();

  constructor(private httpService: HttpService) {}

  ngOnInit() {
  	this.httpService.getDepartmentsResponse().subscribe(
      response => {
          this.httpStatus = response.status;
          this.departmentsData = response.json();
        },
        status => {
          this.httpStatus = status;
        }
    );
  }

  onSubmit() {
  	if (this.employee.isNotValid()) {
  		return;
  	}
    this.httpService.createEmployee(this.employee);
  }
}