import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response } from '@angular/http';
import { HttpService } from '../http.service';
import { Observable } from 'rxjs/Rx';
import { Employee } from './employee';

@Component({
  selector: 'app-add-employee',
  templateUrl: './view-employee.component.html',
  providers: [HttpService]
})
export class ViewEmployeeComponent implements OnInit {
  employeeId: number;

  httpStatus: number;

  employee = new Employee();

  constructor(private httpService: HttpService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.employeeId = params['id'];
    });

  	this.httpService.getEmployeeResponse(this.employeeId).subscribe (
      response => {
        this.httpStatus = response.status;
        this.employee = response.json();
        console.log(this.employee);
      },
      status => {
        this.httpStatus = status;
      }
    );
  }

  isActive (active: boolean) {
    return active ? 'Yes' : 'No';
  }
}