export class Employee {
    id: number;

    name: string;

    active: boolean;

    departmentId: number;

    departmentName: string;

    isNotValid () {
		if (!this.name) {
			alert('You must enter Name');
			return true;
		}
		if (!this.departmentId) {
			alert('You must choose Department');
			return true;
		}
    	return false;
    }
}