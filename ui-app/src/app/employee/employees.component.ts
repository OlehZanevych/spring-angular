import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response } from '@angular/http';
import { HttpService } from '../http.service';
import { Observable } from 'rxjs/Rx';
import { PagedResult } from '../paged-result';
import { Employee } from './employee';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
  providers: [HttpService]
})
export class EmployeesComponent implements OnInit {
  page: number;

  pageIndexes: Array<number>;

  perPageCount = 10;

  httpStatus: number;

  data: PagedResult<Employee> = new PagedResult<Employee>();

  pageCount: number;

  offset: number;

  search = '';

  newSearch: string;

  constructor(private httpService: HttpService, private route: ActivatedRoute,
    private router: Router) {}

  retrieveData() {
    this.httpService.getEmployeesResponse(this.offset, this.perPageCount, this.search).subscribe (
      response => {
        this.httpStatus = response.status;
        this.data = response.json();
        this.pageCount = Math.ceil(this.data.count / this.perPageCount);
          
        if (this.page > 1 && this.page > this.pageCount) {
          this.router.navigate([this.httpService.employeesPageUrl],
            {queryParams: {page: this.pageCount}});
        }

        if (this.pageCount > 1) {
          this.pageIndexes = [];
          for (var i = 1; i <= this.pageCount; ++i) {
            this.pageIndexes.push(i);
          }
        }
      },
      status => {
        this.httpStatus = status;
      }
    );
  }

  ngOnInit() {
    this.route.queryParams.subscribe(queryParams => {
      this.page = queryParams['page'];
      if (!this.page) {
        this.page = 1;
      }
      if (this.page < 1) {
        this.page = 1;
      }

      this.offset = (this.page - 1) * this.perPageCount;

      this.search = queryParams['search'];
      this.verifySearch();
      this.newSearch = this.search;

      this.retrieveData();
    });
  }

  onDelete(employeeId: number) {
    this.httpService.deleteEmployee(employeeId).subscribe (
      response => {
        this.retrieveData();
      },
      errorStatus => {
        this.httpService.processErrorStatus(errorStatus);
      }
    );
  }

  verifySearch() {
    if (this.search) {
      this.search = this.search.trim().replace(/\s+/g, ' ');
    }
  }

  processSearch() {
    this.search = this.newSearch;
    this.verifySearch();
    var queryParams = {};
    if (this.search) {
      queryParams = {search: this.search};
    }
    this.router.navigate([this.httpService.employeesPageUrl],
      {queryParams: queryParams});
    this.newSearch = this.search;
  }

  getQueryParams(page: number) {
    if (this.search) {
      return {search: this.search, page: page};
    } else {
      return {page: page};
    }
  }

  isActive (active: boolean) {
  	return active ? 'Yes' : 'No';
  }
}