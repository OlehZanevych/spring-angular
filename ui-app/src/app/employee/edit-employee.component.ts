import { Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Response } from '@angular/http';
import { HttpService } from '../http.service';
import { Observable } from 'rxjs/Rx';
import { PagedResult } from '../paged-result';
import { Department } from '../department/department';
import { Employee } from './employee';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-edit-employee.component.html',
  styleUrls: ['./add-edit-employee.component.css'],
  providers: [HttpService]
})
export class EditEmployeeComponent implements OnInit {
  title = 'Edit Employee';

  submitOperation = 'Save';

  employeeId: number;

  httpStatus: number;

  departmentsData: PagedResult<Department>;

  employee = new Employee();

  constructor(private httpService: HttpService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.employeeId = params['id'];
    });

  	this.httpService.getDepartmentsResponse().subscribe(
      response => {
          this.departmentsData = response.json();

          this.httpService.getEmployeeResponse(this.employeeId).subscribe (
            response => {
              this.httpStatus = response.status;
              this.employee = response.json();
            },
            status => {
              this.httpStatus = status;
            }
          );
        },
        status => {
          this.httpStatus = status;
        }
    );
  }

  onSubmit() {
    if (!this.employee.name) {
      alert('You must enter Name');
      return;
    }
    if (!this.employee.departmentId) {
      alert('You must choose Department');
      return;
    }
    this.httpService.updateEmployee(this.employee);
  }
}