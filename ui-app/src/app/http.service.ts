import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { PagedResult } from './paged-result';
import { Employee } from './employee/employee';
 
@Injectable()
export class HttpService {

	serviceUrl = 'http://localhost:8080/';

    headers = new Headers ({'Content-Type': 'application/json'});

    options = new RequestOptions({headers: this.headers});

    departmentsUrl = this.serviceUrl + 'departments/';

	employeesUrl = this.serviceUrl + 'employees/';

    employeesPageUrl = './employees';

    editEmployeePageUrl = './edit-employee/';

    connectingErrorMessage = 'Unable connecting to Server';

    sendingErrorMessage = 'An error occurred while sending data to Server';
 
    constructor(private http: Http, private router: Router) {}

    private extractData(res: Response) {
        return res;
    }

    private handleError(error: Response) {
        return Observable.throw(error.status);
    }

    public processErrorStatus(errorStatus: number) {
        errorStatus === 0 ? alert(this.connectingErrorMessage)
            : alert(this.sendingErrorMessage);
    }

    public get(url: string) {
        return this.http.get(url).map(this.extractData)
            .catch(this.handleError);
    }

    public post(url: string, data: any) {
        return this.http.post(url, data, this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    public put(url: string, data: any) {
        return this.http.put(url, data, this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    public delete(url: string) {
        return this.http.delete(url, this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    public getDepartmentsResponse () {
        return this.get(this.departmentsUrl);
    }
     
    public getEmployeesResponse(offset: number, limit: number, search: string) {
        var url = this.employeesUrl + '?offset=' + offset + '&limit=' + limit;
        if (search) {
            url = url + '&search=' + search;
        }

        return this.get(url);
    }

    public getEmployeeResponse(employeeId: number) {
        return this.get(this.employeesUrl + employeeId);
    }

    public createEmployee(employee: Employee) {
        this.post(this.employeesUrl, employee).subscribe (
            response => {
                alert('Employee successfully added');
                this.router.navigate([this.editEmployeePageUrl + response.json().id]);
            },
            errorStatus => {
                this.processErrorStatus(errorStatus);
            }
        )
    }

    public updateEmployee(employee: Employee) {
        this.put(this.employeesUrl + employee.id, employee).subscribe (
            response => {
                alert('Employee successfully updated');
            },
            errorStatus => {
                this.processErrorStatus(errorStatus);
            }
        )
    }

    public deleteEmployee(employeeId: number) {
        return this.delete(this.employeesUrl + employeeId);
    }
}