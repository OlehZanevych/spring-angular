export class PagedResult<INSTANCE> {
    offset: number;
    limit: number;
    count: number;
    instances: INSTANCE[];
}