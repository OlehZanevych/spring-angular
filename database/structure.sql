SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

CREATE TABLE IF NOT EXISTS t_essence_departments (
  id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name varchar(255) COLLATE cp1251_ukrainian_ci DEFAULT NULL,
  FULLTEXT (name)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci;

CREATE TABLE IF NOT EXISTS t_essence_employees (
  id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  is_active bit(1) DEFAULT NULL,
  name varchar(255) COLLATE cp1251_ukrainian_ci DEFAULT NULL,
  department_id bigint(20) DEFAULT NULL,
  FOREIGN KEY (department_id) REFERENCES t_essence_departments (id),
  FULLTEXT (name)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci;