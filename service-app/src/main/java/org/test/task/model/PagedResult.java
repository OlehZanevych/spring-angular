package org.test.task.model;

import java.util.List;

public class PagedResult<INSTANCE> {
	
	private int offset;
    
	private Integer limit;
    
	private long count;
    
	private List<INSTANCE> instances;

	public PagedResult(final int offset, final Integer limit, final long count,
			final List<INSTANCE> instances) {
		
		this.offset = offset;
		this.limit = limit;
		this.count = count;
		this.instances = instances;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(final int offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(final Integer limit) {
		this.limit = limit;
	}

	public long getCount() {
		return count;
	}

	public void setCount(final long count) {
		this.count = count;
	}

	public List<INSTANCE> getInstances() {
		return instances;
	}

	public void setInstances(final List<INSTANCE> instances) {
		this.instances = instances;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (count ^ (count >>> 32));
		result = prime * result + ((instances == null) ? 0 : instances.hashCode());
		result = prime * result + ((limit == null) ? 0 : limit.hashCode());
		result = prime * result + offset;
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PagedResult<?> other = (PagedResult<?>) obj;
		if (count != other.count) {
			return false;
		}
		if (instances == null) {
			if (other.instances != null) {
				return false;
			}
		} else if (!instances.equals(other.instances)) {
			return false;
		}
		if (limit == null) {
			if (other.limit != null) {
				return false;
			}
		} else if (!limit.equals(other.limit)) {
			return false;
		}
		if (offset != other.offset) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PagedResult [offset=");
		builder.append(offset);
		builder.append(", limit=");
		builder.append(limit);
		builder.append(", count=");
		builder.append(count);
		builder.append(", instances=");
		builder.append(instances);
		builder.append("]");
		return builder.toString();
	}

}
