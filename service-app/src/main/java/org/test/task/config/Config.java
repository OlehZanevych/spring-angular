package org.test.task.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.test.task.converter.department.DepartmentConverter;
import org.test.task.converter.department.DepartmentResourceConverter;
import org.test.task.converter.employee.EmployeeConverter;
import org.test.task.converter.employee.EmployeeResourceConverter;
import org.test.task.facade.DefaultFacade;
import org.test.task.facade.Facade;
import org.test.task.model.Department;
import org.test.task.model.Employee;
import org.test.task.repository.Repository;
import org.test.task.resource.DepartmentResource;
import org.test.task.resource.EmployeeResource;

@Configuration
@ComponentScan("org.test.task")
public class Config {
	
	@Autowired
	ApplicationContext context;
	
	@Bean
	public Repository<Department> departmentRepository() {
		return new Repository<Department>(Department.class);
	}
	
	@Bean
	public Repository<Employee> employeeRepository() {
		return new Repository<Employee>(Employee.class);
	}
	
	@Bean
	@SuppressWarnings("unchecked")
	public Facade<DepartmentResource> departmentFacade() {
		DefaultFacade<DepartmentResource, Department, DepartmentConverter,
				DepartmentResourceConverter> defaultFacade = new DefaultFacade<>();
		
		defaultFacade.setRepository(
				(Repository<Department>) context.getBean("departmentRepository"));
		
		defaultFacade.setEntityConverter(context.getBean(DepartmentConverter.class));
		
		defaultFacade.setResourceConverter(context.getBean(DepartmentResourceConverter.class));
		
		return defaultFacade;
	}
	
	@Bean
	@SuppressWarnings("unchecked")
	public Facade<EmployeeResource> employeeFacade() {
		DefaultFacade<EmployeeResource, Employee, EmployeeConverter,
				EmployeeResourceConverter> defaultFacade = new DefaultFacade<>();
		
		defaultFacade.setRepository(
				(Repository<Employee>) context.getBean("employeeRepository"));
		
		defaultFacade.setEntityConverter(context.getBean(EmployeeConverter.class));
		
		defaultFacade.setResourceConverter(context.getBean(EmployeeResourceConverter.class));
		
		return defaultFacade;
	}
}
