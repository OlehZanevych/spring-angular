package org.test.task.exception;

import org.test.task.model.Model;

/**
 * Custom exception, that mean, that entity doesn't exists.
 * 
 * @author OlehZanevych
 */
public class EntityNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public EntityNotFoundException (final Class<? extends Model> entityClass,
			final Long entityId) {
		
		super(String.format("%s with id %d doesn't exist",
				entityClass.getSimpleName(), entityId));
	}

}
