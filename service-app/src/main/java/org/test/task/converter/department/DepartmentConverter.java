package org.test.task.converter.department;

import org.springframework.stereotype.Component;
import org.test.task.converter.Converter;
import org.test.task.model.Department;
import org.test.task.resource.DepartmentResource;

@Component("departmentConverter")
public class DepartmentConverter extends Converter<Department, DepartmentResource> {

	@Override
	public DepartmentResource convert(final Department source, final DepartmentResource target) {
		target.setId(source.getId());
		
		target.setName(source.getName());
		
		return target;
	}

	@Override
	public DepartmentResource convert(final Department source) {
		return convert(source, new DepartmentResource());
	}

}
