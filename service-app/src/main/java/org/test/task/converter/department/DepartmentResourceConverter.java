package org.test.task.converter.department;

import org.springframework.stereotype.Component;
import org.test.task.converter.Converter;
import org.test.task.model.Department;
import org.test.task.resource.DepartmentResource;

@Component("departmentResourceConverter")
public class DepartmentResourceConverter extends Converter<DepartmentResource, Department> {

	@Override
	public Department convert(DepartmentResource source, Department target) {
		target.setName(source.getName());
		
		return target;
	}

	@Override
	public Department convert(final DepartmentResource source) {
		return convert(source, new Department());
	}

}
