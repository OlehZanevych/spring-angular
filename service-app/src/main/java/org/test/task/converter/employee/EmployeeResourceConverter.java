package org.test.task.converter.employee;

import org.springframework.stereotype.Component;
import org.test.task.converter.Converter;
import org.test.task.model.Department;
import org.test.task.model.Employee;
import org.test.task.resource.EmployeeResource;

@Component("employeeResourceConverter")
public class EmployeeResourceConverter extends Converter<EmployeeResource, Employee> {

	@Override
	public Employee convert(EmployeeResource source, Employee target) {
		target.setName(source.getName());
		
		target.setActive(source.isActive());
		
		target.setDepartment(new Department(source.getDepartmentId()));
		
		return target;
	}

	@Override
	public Employee convert(final EmployeeResource source) {
		return convert(source, new Employee());
	}

}
