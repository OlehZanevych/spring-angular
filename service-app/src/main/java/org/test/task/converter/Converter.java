package org.test.task.converter;

import java.util.ArrayList;
import java.util.List;


/**
 * Abstract class for converting one object into another.
 * @author OlehZanevych
 *
 * @param <SOURCE> Input object class
 * @param <TARGET> Output object class
 */
public abstract class Converter<SOURCE, TARGET> {
	
	/**
	 * Method is used to convert from one object to Another.
	 * Note, that another object should be already instantiated.
	 * @param source From.
	 * @param target To.
	 * @return Converted object.
	 */
	public abstract TARGET convert(final SOURCE source, final TARGET target);
    
    /**
     * Classic method, that is used to convert from one instance
     * to another.
     * @param source From what to convert.
     * @return Converted object.
     */
	public abstract TARGET convert(final SOURCE source);
	
	/**
     * Converts List of objects.
     * @param sources List input objects.
     * @return Converted List of Output objects.
     */
	public List<TARGET> convertAll(final List<SOURCE> sources) {
    	if (sources == null) {
    		return null;
    	}
        return convertAll(sources, new ArrayList<TARGET>(sources.size()));
    }
	
	/**
     * Converts List of objects.
     * targets should be already instantiated.
     * @param sources List input objects.
     * @param targets List of Output objects
     * @return Converted List of Output objects.
     */
	public List<TARGET> convertAll(final List<SOURCE> sources, final List<TARGET> targets) {
        for (SOURCE source : sources) {
            targets.add(convert(source));
        }
        return targets;
    }

}
