package org.test.task.converter.employee;

import org.springframework.stereotype.Component;
import org.test.task.converter.Converter;
import org.test.task.model.Department;
import org.test.task.model.Employee;
import org.test.task.resource.EmployeeResource;

@Component("employeeConverter")
public class EmployeeConverter extends Converter<Employee, EmployeeResource> {

	@Override
	public EmployeeResource convert(final Employee source, final EmployeeResource target) {
		target.setId(source.getId());
		
		target.setName(source.getName());
		
		target.setActive(source.isActive());
		
		Department department = source.getDepartment();
		if (department != null) {
			target.setDepartmentId(department.getId());
			
			target.setDepartmentName(department.getName());
		}
		
		return target;
	}

	@Override
	public EmployeeResource convert(final Employee source) {
		return convert(source, new EmployeeResource());
	}

}
