package org.test.task.facade;

import org.test.task.model.PagedResult;
import org.test.task.resource.Request;
import org.test.task.resource.Resource;

/**
 * Common interface for facade methods
 * of each entity resource.
 * @author OlehZanevych
 *
 * @param <RESOURCE> Resource class.
 */
public interface Facade<RESOURCE extends Resource> {
	
	/**
	 * Method for creating entity.
	 * @param resource
	 * @return Entity resource.
	 */
	RESOURCE createResource(RESOURCE resource);
	
	/**
	 * Method for updating entity.
	 * @param id id
	 * @param resource resource
	 */
	void updateResource(Long id, RESOURCE resource);

	/**
	 * Method for removing entity.
	 * @param id id
	 */
	void removeResource(Long id);
	
	/**
	 * Method for getting entity.
	 * @param id id
	 * @param request Request
	 * @return Entity resource.
	 */
	RESOURCE getResource(Long id);
	
	/**
	 * Method for getting paged result for entities.
	 * @param request PagedRequest
	 * @return Paged Result.
	 */
	PagedResult<RESOURCE> getResources(Request request);

}
