package org.test.task.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.transaction.annotation.Transactional;
import org.test.task.converter.Converter;
import org.test.task.model.Model;
import org.test.task.model.PagedResult;
import org.test.task.repository.Repository;
import org.test.task.resource.Request;
import org.test.task.resource.Resource;

@Transactional
public class DefaultFacade<RESOURCE extends Resource, ENTITY extends Model,
		ENTITYCONVERTER extends Converter<ENTITY, RESOURCE>,
		RESOURCECONVERTER extends Converter<RESOURCE, ENTITY>>
		implements Facade<RESOURCE> {
	
	protected Repository<ENTITY> repository;
	
	protected ENTITYCONVERTER entityConverter;
	
	protected RESOURCECONVERTER resourceConverter;

	@Override
	public RESOURCE createResource(RESOURCE resource) {
		ENTITY entity = repository.add(resourceConverter.convert(resource));
		
		return entityConverter.convert(entity);
	}

	@Override
	public void updateResource(Long id, RESOURCE resource) {
		ENTITY entity = repository.findById(id);
		
		resourceConverter.convert(resource, entity);
		
		repository.update(entity);
	}

	@Override
	public void removeResource(Long id) {
		repository.remove(id);
	}

	@Override
	public RESOURCE getResource(Long id) {
		ENTITY entity = repository.findById(id);
		
		return entityConverter.convert(entity);
	}

	@Override
	public PagedResult<RESOURCE> getResources(Request request) {
		PagedResult<ENTITY> pagedResult = repository.find(request);
		
		List<RESOURCE> resources = entityConverter.convertAll(pagedResult.getInstances());
		
		return new PagedResult<RESOURCE>(pagedResult.getOffset(), pagedResult.getLimit(),
				pagedResult.getCount(), resources);
	}

	@Required
	public void setRepository(final Repository<ENTITY> repository) {
		this.repository = repository;
	}

	@Required
	public void setEntityConverter(ENTITYCONVERTER entityConverter) {
		this.entityConverter = entityConverter;
	}

	@Required
	public void setResourceConverter(RESOURCECONVERTER resourceConverter) {
		this.resourceConverter = resourceConverter;
	}

}
