package org.test.task.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.test.task.facade.Facade;
import org.test.task.model.PagedResult;
import org.test.task.resource.Message;
import org.test.task.resource.Request;
import org.test.task.resource.Resource;

public class Controller<RESOURCE extends Resource> {
	
	private Facade<RESOURCE> facade;
	
	/**
	 * Method for creating new Resource.
	 * @param resource Resource instance.
	 * @return Resource with generated identifier.
	 */
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public RESOURCE createResource(@RequestBody final RESOURCE resource) {
		return facade.createResource(resource);
	}
	
	/**
	 * Method for updating Resource.
	 * @param id Resource identifier.
	 * @param resource updated Resource.
	 * @return notification of update Resource.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Message updateResource(@PathVariable("id") final Long id, @RequestBody final RESOURCE resource) {
		facade.updateResource(id, resource);
		return new Message("Resource updated");
	}

	/**
	 * Method for getting Resource.
	 * @param id Resource identifier.
	 * @return Resource instance.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public RESOURCE getResource(@PathVariable("id") final Long id) {
		return facade.getResource(id);
	}
	
	/**
	 * Method for removing Resource.
	 * @param id Resource identifier.
	 * @return notification of deletion Resource.
	 */
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Message removeResource(@PathVariable("id") final Long id) {
		facade.removeResource(id);
		return new Message("Resource removed");
	}
	
	/**
	 * Method that returns a page of Resources.
	 * @param request request.
	 * @return page of Resources.
	 */
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET)
	public PagedResult<RESOURCE> getPagedResources(final Request request) {
		return facade.getResources(request);
	}
	
	protected void setFacade(Facade<RESOURCE> facade) {
		this.facade = facade;
	}
}
