package org.test.task.controller.resolver;


import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.test.task.resource.Request;

import java.util.Map;

/**
 * Paged request argument resolver.
 * 
 * @author OlehZanevych
 */
@Component("requestHandlerMethodArgumentResolver")
public class RequestHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
	
	@Override
	public boolean supportsParameter(final MethodParameter parameter) {
		return Request.class.isAssignableFrom(parameter.getParameterType());
	}

	@Override
	public Object resolveArgument(final MethodParameter parameter,
			final ModelAndViewContainer mavContainer, final NativeWebRequest webRequest,
			final WebDataBinderFactory binderFactory) throws Exception {
		
		Map<String, String[]> parameters = webRequest.getParameterMap();
		
		Request request = new Request();
		
		String[] offsetValue = parameters.get("offset");
		if (offsetValue != null) {
			request.setOffset(Integer.parseInt(offsetValue[0]));
		}
		
		String[] limitValue = parameters.get("limit");
		if (limitValue != null) {
			request.setLimit(Integer.parseInt(limitValue[0]));
		}
		
		String[] searchValue = parameters.get("search");
		if (searchValue != null) {
			request.setSearch(searchValue[0].trim().replaceAll("\\s+", " "));
		}
		
		return request;
	}

}
