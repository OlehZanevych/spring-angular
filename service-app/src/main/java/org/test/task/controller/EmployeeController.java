package org.test.task.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.test.task.facade.Facade;
import org.test.task.resource.EmployeeResource;

/**
 * Controller, that handles all API with Employee.
 * 
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/employees")
public class EmployeeController extends Controller<EmployeeResource> {
	
	@Autowired
	ApplicationContext context;
	
	@PostConstruct
	@SuppressWarnings("unchecked")
	private void init() {
		setFacade((Facade<EmployeeResource>) context.getBean("employeeFacade"));
	}

}
