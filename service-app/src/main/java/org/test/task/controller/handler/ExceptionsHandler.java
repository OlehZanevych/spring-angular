package org.test.task.controller.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.test.task.exception.EntityNotFoundException;
import org.test.task.resource.Message;

/**
 * Handlers for handling Exceptions.
 * 
 * @author OlehZanevych
 */
@ControllerAdvice
public class ExceptionsHandler {
	
	@ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    public Message handleEntityNotFoundException(final EntityNotFoundException e) {
    	return new Message(e);
    }
	
	@ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
    public Message defaultHandleException(final Exception e) {
    	return new Message(e);
    }
}
