package org.test.task.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.test.task.facade.Facade;
import org.test.task.resource.DepartmentResource;

/**
 * Controller, that handles all API with Department.
 * 
 * @author OlehZanevych
 */
@RestController
@RequestMapping("/departments")
public class DepartmentController extends Controller<DepartmentResource> {
	
	@Autowired
	ApplicationContext context;
	
	@PostConstruct
	@SuppressWarnings("unchecked")
	private void init() {
		setFacade((Facade<DepartmentResource>) context.getBean("departmentFacade"));
	}

}
