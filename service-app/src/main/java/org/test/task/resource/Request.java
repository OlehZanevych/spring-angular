package org.test.task.resource;

/**
 * Request, that comes from controller.
 * 
 * @author OlehZanevych
 */
public class Request {
	
	protected int offset;
	
	protected Integer limit;
	
	protected String search;

	public int getOffset() {
		return offset;
	}

	public void setOffset(final int offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(final Integer limit) {
		this.limit = limit;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(final String search) {
		this.search = search;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((limit == null) ? 0 : limit.hashCode());
		result = prime * result + offset;
		result = prime * result + ((search == null) ? 0 : search.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Request other = (Request) obj;
		if (limit == null) {
			if (other.limit != null) {
				return false;
			}
		} else if (!limit.equals(other.limit)) {
			return false;
		}
		if (offset != other.offset) {
			return false;
		}
		if (search == null) {
			if (other.search != null) {
				return false;
			}
		} else if (!search.equals(other.search)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Request [offset=");
		builder.append(offset);
		builder.append(", limit=");
		builder.append(limit);
		builder.append(", search=");
		builder.append(search);
		builder.append("]");
		return builder.toString();
	}

}
