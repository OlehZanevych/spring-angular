package org.test.task.repository;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.test.task.exception.EntityNotFoundException;
import org.test.task.model.Model;
import org.test.task.model.PagedResult;
import org.test.task.resource.Request;

public class Repository<ENTITY extends Model> {
	
	private static final String DELETE_QUERY = "DELETE %s WHERE id = :id";
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	protected Class<ENTITY> clazz;
	
	public Repository(Class<ENTITY> clazz) {
		this.clazz = clazz;
	}

	public ENTITY add(final ENTITY entity) {
		entityManager.persist(entity);
		return entity;
	}
	
	public ENTITY update(final ENTITY entity) {
        return entityManager.merge(entity);
    }
	
	public void remove(final Long id) {
		String deleteSQL = String.format(DELETE_QUERY, clazz.getName());
		int count = entityManager.createQuery(deleteSQL).setParameter("id", id).executeUpdate();
		if (count == 0) {
			throw new EntityNotFoundException(clazz, id);
    	}
    }
	
	public ENTITY findById(final Long id) {
		ENTITY entity = entityManager.find(clazz, id);
		if (entity == null) {
			throw new EntityNotFoundException(clazz, id);
		}
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	public PagedResult<ENTITY> find(final Request request) {
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(clazz)
				.addOrder(Order.asc("name").ignoreCase());
		
		String search = request.getSearch();
		if (search != null) {
			Criterion[] predicates = Arrays.stream(search.split(" ")).map(i ->
					Restrictions.ilike("name", i, MatchMode.ANYWHERE)).toArray(Criterion[]::new);
			criteria.add(Restrictions.and(predicates));
		}
		
		Integer offset = request.getOffset();
		
		Integer limit = request.getLimit();
		
		long count = 0;
		
		List<ENTITY> instances = null;
		
		if (offset != 0 || limit != null) {
			count = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
		
			if (offset != 0) {
				criteria.setFirstResult(offset);
			}
			
			if (limit != null) {
				criteria.setMaxResults(limit);
			}
			
			instances =  criteria.setProjection(null).list();
		} else {
			instances =  criteria.list();
			count = instances.size();
		}
		
		return new PagedResult<>(offset, limit, count, instances);
	}
}
